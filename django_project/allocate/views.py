import json

from django.http import HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from allocation import services
from allocation.adapters.django import DjangoUnitOfWork


@csrf_exempt
def index(request: HttpRequest):
    """Allocate input order line to a batch.
    """
    if request.method != "POST":
        return JsonResponse({"error": "Page not found"}, status=404)

    uow = DjangoUnitOfWork()
    data = json.loads(request.body)

    try:
        batch_ref = services.allocate(data["order_id"], data["sku"], data["qty"], uow)
    except (services.InvalidSKU, services.OutOfStock) as err:
        return JsonResponse({"error": str(err)}, status=400)

    return JsonResponse({"batch_ref": batch_ref})


@csrf_exempt
def batch(request: HttpRequest):
    """Create or update a simple batch
    """
    if request.method not in ("POST", "PUT"):
        return JsonResponse({"error": "Page not found"}, status=404)

    uow = DjangoUnitOfWork()
    data = json.loads(request.body)
    if request.method == "POST":
        services.add_batch(data["ref"], data["sku"], data["qty"], data["eta"], uow)
        return JsonResponse({"info": f"Batch {data['ref']} successfully created"})
    else:
        services.update_batch(data["ref"], data["qty"], uow)
        return JsonResponse({"info": f"Batch {data['ref']} successfully updated"})
