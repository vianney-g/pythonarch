from django.db import models


class Order(models.Model):
    """Order ORM.
    """

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return f"Order <{self.id}>"

    class Meta:
        db_table = "order"


class Batch(models.Model):
    """Batch ORM.
    """

    reference = models.CharField(max_length=250, primary_key=True)
    sku = models.CharField(max_length=250)
    eta = models.DateField(null=True)
    qty = models.IntegerField()

    def __repr__(self):
        return f"Batch <{self.reference}>: {self.sku} x {self.qty}"

    class Meta:
        db_table = "batch"


class OrderLine(models.Model):
    """OrderLine ORM.
    """

    sku = models.CharField(max_length=250)
    qty = models.IntegerField()
    order = models.ForeignKey(Order, related_name="lines", on_delete=models.CASCADE)
    batches = models.ManyToManyField(Batch, related_name="order_lines")

    def __repr__(self):
        return f"Order line <{self.id}>: {self.sku} x {self.qty}"

    class Meta:
        db_table = "order_line"
