from typing import Iterable

from allocate import models
from allocation import domain
from .base import AbstractRepository


class DjangoRepository(AbstractRepository):
    def add(self, batch: domain.Batch):
        self.update(batch)

    def update(self, batch: domain.Batch):
        models.Batch.update_from_domain(batch)

    def get(self, reference: str) -> domain.Batch:
        return models.Batch.objects.get(reference=reference).to_domain()

    def list(self) -> Iterable[domain.Batch]:
        for batch in models.Batch.objects.all():
            yield batch.to_domain()
