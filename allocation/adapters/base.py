import abc
from typing import Iterable, Optional

from allocation import domain


class AbstractSession(abc.ABC):
    """Abstraction of db session.
    """

    @abc.abstractmethod
    def commit(self):
        ...


class AbstractProductsRepository(abc.ABC):
    """Abstraction of a product repository.
    """

    @abc.abstractmethod
    def add(self, product: domain.Product):
        ...

    @abc.abstractmethod
    def update(self, product: domain.Product):
        ...

    @abc.abstractmethod
    def get(self, sku: str) -> domain.Product:
        ...

    @abc.abstractmethod
    def get_for_batch(self, batch_ref: str) -> Optional[domain.Product]:
        ...


class AbstractUnitOfWork(abc.ABC):
    """Unit of work db pattern.
    """

    products: AbstractProductsRepository

    @abc.abstractmethod
    def __enter__(self) -> "AbstractUnitOfWork":
        ...

    def __exit__(self, *args):
        self.rollback()

    @abc.abstractmethod
    def commit(self):
        ...

    @abc.abstractmethod
    def rollback(self):
        ...
