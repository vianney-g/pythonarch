"""Boilerplate to convert domain to django models.
"""
from functools import singledispatch
from typing import Iterable, Optional

from django.db import transaction
from django.db.models import Model as DjangoModel

from allocate import models
from allocation import domain

from .base import AbstractProductsRepository, AbstractSession, AbstractUnitOfWork


@singledispatch
def to_domain(model: DjangoModel) -> "Domain model":
    """Convert a django model to domain model.
    """
    raise NotImplementedError


@to_domain.register
def _(model: models.Batch) -> domain.Batch:
    """Convert django batch to domain batch.
    """
    domain_batch = domain.Batch(
        ref=model.reference, sku=model.sku, qty=model.qty, eta=model.eta
    )
    for line in model.order_lines.all():
        domain_batch.allocate(to_domain(line))

    return domain_batch


@to_domain.register
def _(model: models.OrderLine) -> domain.OrderLine:
    """Convert django line to domain order line.
    """
    return domain.OrderLine(orderid=model.order_id, sku=model.sku, qty=model.qty)


@singledispatch
def update_django(domain_object) -> Optional[DjangoModel]:
    """Update django model from domain model.
    """
    raise NotImplementedError


@update_django.register
def _(product: domain.Product) -> None:
    """Update django model from product.
    """
    for batch in product.batches:
        update_django(batch)


@update_django.register
def _(domain_batch: domain.Batch) -> models.Batch:
    """Update django from domain batch.
    """
    batch, _ = models.Batch.objects.update_or_create(
        reference=domain_batch.reference,
        defaults={
            "sku": domain_batch.sku,
            "eta": domain_batch.eta,
            "qty": domain_batch.purchased_quantity,
        },
    )

    batch.order_lines.set(update_django(line) for line in domain_batch.allocations)
    return batch


@update_django.register
def _(domain_order_line: domain.OrderLine) -> models.OrderLine:
    """Update django from domain order_line.
    """
    order, _ = models.Order.objects.get_or_create(id=domain_order_line.orderid)
    return models.OrderLine.objects.create(
        order=order, sku=domain_order_line.sku, qty=domain_order_line.qty
    )


class DjangoSession(AbstractSession):
    def __init__(self):
        self._seen = set()

    def add(self, product: domain.Product):
        self._seen.add(product)

    def commit(self):
        while self._seen:
            product = self._seen.pop()
            update_django(product)


class DjangoProductsRepository(AbstractProductsRepository):
    """Repository adapter for django.
    """

    def __init__(self, session: DjangoSession):
        self._session = session

    def add(self, product: domain.Batch):
        self.update(product)

    def update(self, product: domain.Batch):
        self._session.add(product)
        update_django(product)

    def get(self, sku: str) -> domain.Product:
        model_batches = models.Batch.objects.filter(sku=sku)
        product = domain.Product(sku=sku, batches=[to_domain(b) for b in model_batches])
        self._session.add(product)
        return product

    def get_for_batch(self, batch_ref: str) -> domain.Product:
        try:
            batch = models.Batch.objects.get(reference=batch_ref)
        except models.Batch.DoesNotExist:
            return None
        return self.get(batch.sku)


class DjangoUnitOfWork(AbstractUnitOfWork):
    """Uow implementation for Django.
    """

    def __init__(self):
        self.session = DjangoSession()
        self.products = DjangoProductsRepository(self.session)

    def __enter__(self) -> AbstractUnitOfWork:
        transaction.set_autocommit(False)
        return self

    def __exit__(self, *args):
        super().__exit__(*args)
        transaction.set_autocommit(True)

    def commit(self):
        self.session.commit()
        transaction.commit()

    def rollback(self):
        transaction.rollback()
