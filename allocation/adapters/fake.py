import abc
from typing import Iterable, Optional

from allocation import domain
from .base import AbstractProductsRepository, AbstractUnitOfWork


class FakeProductsRepository(AbstractProductsRepository):
    """Simple in memory repository for tests purposes.
    """

    @classmethod
    def for_batch(cls, ref, sku, qty, eta=None):
        """Factory for a single batch repository.
        """
        batches = [domain.Batch(ref, sku, qty, eta)]
        return cls([domain.Product(sku, batches)])

    def __init__(self, products: Iterable[domain.Product] = None):
        products = products or []
        self._products = {product.sku: product for product in products}

    def add(self, product: domain.Product):
        self.update(product)

    def update(self, product: domain.Product):
        self._products[product.sku] = product

    def get(self, sku: str) -> domain.Product:
        if sku not in self._products:
            self._products[sku] = domain.Product(sku)
        return self._products[sku]

    def get_for_batch(self, batch_ref: str) -> Optional[domain.Product]:
        for product in self._products.values():
            if any(b.reference == batch_ref for b in product.batches):
                return product


class FakeUnitOfWork(AbstractUnitOfWork):
    def __init__(self, repo: Optional[FakeProductsRepository] = None):
        if not repo:
            repo = FakeProductsRepository([])
        self.products = repo
        self.committed = False

    def __enter__(self) -> AbstractUnitOfWork:
        self.committed = False
        return self

    def commit(self):
        self.committed = True

    def rollback(self):
        pass
