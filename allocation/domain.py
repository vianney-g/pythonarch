"""Domain models
"""
from dataclasses import dataclass
from datetime import date
from typing import Iterable, Optional, Set


class OutOfStock(Exception):
    """Cannot allocate line.
    """


@dataclass(frozen=True)
class OrderLine:
    orderid: str
    sku: str
    qty: int


class Batch:
    """Model class representing a batch with allocated order lines.
    """

    def __init__(self, ref: str, sku: str, qty: int, eta: Optional[date]):
        self.reference = ref
        self.sku = sku
        self.eta = eta
        self._purchased_quantity = qty
        self._allocations: Set[OrderLine] = set()

    @property
    def allocations(self) -> Iterable[OrderLine]:
        """Get all allocations (readonly)
        """
        return self._allocations

    def allocate(self, line: OrderLine):
        """Allocate an order line to a batch.
        """
        if self.can_allocate(line):
            self._allocations.add(line)

    def can_allocate(self, line: OrderLine) -> bool:
        """Test if line can be allocated to batch.
        """
        return self.sku == line.sku and self.available_quantity >= line.qty

    @property
    def allocated_quantity(self) -> int:
        return sum(line.qty for line in self._allocations)

    @property
    def available_quantity(self) -> int:
        return self._purchased_quantity - self.allocated_quantity

    @property
    def purchased_quantity(self) -> int:
        return self._purchased_quantity

    @purchased_quantity.setter
    def purchased_quantity(self, qty: int):
        self._purchased_quantity = qty
        while self.available_quantity < 0:
            line = next(iter(self._allocations))
            self.deallocate(line)

    def deallocate(self, line: OrderLine):
        """Deallocate an order line to a batch.
        """
        if line in self._allocations:
            self._allocations.remove(line)

    def __eq__(self, other: "Batch"):
        return isinstance(other, Batch) and self.reference == other.reference

    def __hash__(self):
        return hash(self.reference)

    def __gt__(self, other: "Batch"):
        if self.eta is None:
            return False
        if other.eta is None:
            return True
        return self.eta > other.eta


class Product:
    def __init__(self, sku: str, batches: Optional[Iterable[Batch]] = None):
        self.sku = sku
        self.batches = list(batches or [])

    def add_batch(self, batch):
        self.batches.append(batch)

    def allocate(self, line: OrderLine) -> str:
        """Allocate the line to the best suitable batch.
        Return the chosen batch reference.
        """
        try:
            batch = next(b for b in sorted(self.batches) if b.can_allocate(line))
        except StopIteration:
            raise OutOfStock(f"Out of stock for sku {line.sku}")
        batch.allocate(line)
        return batch.reference
