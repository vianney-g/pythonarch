"""Service layer for order line allocation.
"""

from allocation import domain
from allocation.adapters.base import AbstractUnitOfWork


class InvalidSKU(Exception):
    """The SKU does not exist.
    """


class OutOfStock(Exception):
    """Sku is out of stock"""


def is_valid_sku(sku, batches) -> bool:
    """Check if SKU is known in our repo.
    """
    return any(b.sku == sku for b in batches)


def allocate(order_id: str, sku: str, qty: int, uow: AbstractUnitOfWork) -> str:
    """Allocate the line to the best suitable batch.
    Return the chosen batch reference.
    """
    line = domain.OrderLine(order_id, sku, qty)
    with uow:
        product = uow.products.get(sku)
        if not product.batches:
            raise InvalidSKU(f"Invalid sku {line.sku}")
        try:
            batch_ref = product.allocate(line)
        except domain.OutOfStock as out_of_stock:
            raise OutOfStock(out_of_stock)
        uow.commit()
    return batch_ref


def add_batch(ref: str, sku: str, qty: int, eta: None, uow: AbstractUnitOfWork):
    """Create a batch.
    """
    batch = domain.Batch(ref, sku, qty, eta)
    with uow:
        product = uow.products.get(sku)
        product.add_batch(batch)
        uow.commit()


def update_batch(ref: str, qty: int, uow: AbstractUnitOfWork):
    """Update a batch
    """
    with uow:
        product = uow.products.get_for_batch(ref)
        batch = next(b for b in product.batches if b.reference == ref)
        batch.purchased_quantity = qty
        uow.commit()
