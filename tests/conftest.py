import random
from datetime import date, timedelta
from string import ascii_letters

import pytest


@pytest.fixture
def today():
    return date.today()


@pytest.fixture
def tomorrow(today):
    return today + timedelta(days=1)


@pytest.fixture
def later(tomorrow):
    return tomorrow + timedelta(days=1)


@pytest.fixture
def batch_name():
    """Factory to generate random batch names.
    """

    def batch_name_factory():
        return "".join(random.choice(ascii_letters) for _ in range(15))

    return batch_name_factory
