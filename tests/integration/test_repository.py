from datetime import date

import pytest

from allocate import models
from allocation.adapters.django import DjangoProductsRepository, DjangoSession
from allocation import domain


@pytest.mark.django_db
def test_repository_can_save_a_batch(batch_name):
    batch = domain.Batch(batch_name(), "FRENCH-LAMP", 100, eta=date(2020, 6, 6))
    line = domain.OrderLine(1, "FRENCH-LAMP", 10)

    batch.allocate(line)

    product = domain.Product("FRENCH_LAMP", [batch])

    repo = DjangoProductsRepository(DjangoSession())
    repo.add(product)

    [saved_batch] = models.Batch.objects.all()
    assert saved_batch.reference == batch.reference
    assert saved_batch.sku == batch.sku
    assert saved_batch.eta == batch.eta
    assert saved_batch.qty == batch.purchased_quantity

    [saved_line] = saved_batch.order_lines.all()
    assert saved_line.order_id == line.orderid
    assert saved_line.sku == line.sku
    assert saved_line.qty == line.qty


@pytest.mark.django_db
def test_repository_can_retrieve_an_allocation(batch_name):
    batch1, batch2 = batch_name(), batch_name()
    sku = "FRENCH-LAMP"

    # db init
    dbbatch1 = models.Batch.objects.create(reference=batch1, sku=sku, qty=100, eta=None)
    models.Batch.objects.create(reference=batch2, sku=sku, qty=90, eta=None)
    dborder = models.Order.objects.create()
    dbline = models.OrderLine.objects.create(sku=sku, qty=12, order=dborder)
    dbline.batches.add(dbbatch1)

    repo = DjangoProductsRepository(DjangoSession())
    retrieved = repo.get(sku)

    expected = domain.Product(
        sku,
        [
            domain.Batch(batch1, sku, 100, eta=None),
            domain.Batch(batch2, sku, 90, eta=None),
        ],
    )
    assert retrieved.sku == expected.sku

    for expected_batch, retrieved_batch in zip(
        sorted(expected.batches), sorted(retrieved.batches)
    ):
        assert retrieved_batch._purchased_quantity == expected_batch._purchased_quantity
        if retrieved_batch.reference == batch1:
            assert retrieved_batch._allocations == {
                domain.OrderLine(dborder.id, sku, 12)
            }
