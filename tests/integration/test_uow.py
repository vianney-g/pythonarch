import pytest

from allocate import models
from allocation.adapters.django import DjangoUnitOfWork
from allocation.domain import Batch, OrderLine


def insert_batch(ref, sku, qty, eta):
    """Insert a single batch in db.
    """
    models.Batch.objects.create(reference=ref, sku=sku, qty=qty, eta=eta)


def get_allocated_batch_ref(order_id, sku) -> str:
    """Return the batch ref where line is allocated.
    """
    line = models.OrderLine.objects.get(order_id=order_id, sku=sku)
    batch = line.batches.first()
    return batch.reference


@pytest.mark.django_db(transaction=True)
def test_uow_can_retrieve_a_batch_and_allocate_to_it():
    insert_batch("batch1", "A-LAMP", 100, None)

    with DjangoUnitOfWork() as uow:
        product = uow.products.get(sku="A-LAMP")
        line = OrderLine("1", "A-LAMP", 10)
        product.allocate(line)
        uow.commit()

    batchref = get_allocated_batch_ref("1", "A-LAMP")
    assert batchref == "batch1"
