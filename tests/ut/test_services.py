"""Test the service layer.
"""

import pytest

from allocation import services
from allocation.adapters.fake import FakeProductsRepository, FakeUnitOfWork


def test_returns_allocations():
    """Allocation is performed through service layer.
    """
    uow = FakeUnitOfWork(FakeProductsRepository.for_batch("batch1", "FRENCH-LAMP", 100))
    result = services.allocate("order1", "FRENCH-LAMP", 10, uow)
    assert result == "batch1"


def test_error_for_invalid_sku():
    """Allocation raises error for invalid SKU
    """
    uow = FakeUnitOfWork(FakeProductsRepository.for_batch("batch1", "FRENCH-LAMP", 100))
    with pytest.raises(services.InvalidSKU, match="Invalid sku UNKNOWN-SKU"):
        services.allocate("order1", "UNKNOWN-SKU", 10, uow)


def test_commits():
    """Allocation is DB committed.
    """
    uow = FakeUnitOfWork(FakeProductsRepository.for_batch("batch1", "FRENCH-LAMP", 100))
    services.allocate("order1", "FRENCH-LAMP", 10, uow)
    assert uow.committed


def test_add_batch():
    """Service allows us to create a batch.
    """
    uow = FakeUnitOfWork()
    services.add_batch("batch1", "FRENCH-LAMP", 100, None, uow)
    assert uow.products.get("FRENCH-LAMP").batches
    assert not uow.products.get("UNKNOWN SKU").batches
    assert uow.committed


def test_update_batch_qty():
    uow = FakeUnitOfWork()
    services.add_batch("batch1", "FRENCH-LAMP", 10, None, uow)
    services.allocate("order1", "FRENCH-LAMP", 10, uow)

    [batch] = uow.products.get("FRENCH-LAMP").batches
    assert batch.allocations

    services.update_batch("batch1", qty=8, uow=uow)
    [batch] = uow.products.get("FRENCH-LAMP").batches
    assert not batch.allocations
