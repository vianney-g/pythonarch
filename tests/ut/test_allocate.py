"""Test allocation of a line against a set of batches.
"""
from datetime import date, timedelta

import pytest

from allocation.domain import Batch, OrderLine, OutOfStock, Product


def test_prefers_current_stock_batches_to_shipments(tomorrow):
    in_stock_batch = Batch("in-stock-batch", "RETRO-CLOCK", 100, eta=None)
    shipment_batch = Batch("shipment-batch", "RETRO-CLOCK", 100, eta=tomorrow)

    product = Product("RETRO-CLOCK", [in_stock_batch, shipment_batch])
    line = OrderLine("oref", "RETRO-CLOCK", 10)
    product.allocate(line)

    assert in_stock_batch.available_quantity == 90
    assert shipment_batch.available_quantity == 100


def test_prefers_earlier_batches(today, tomorrow, later):
    earliest = Batch("in-stock-batch", "RETRO-CLOCK", 100, eta=today)
    medium = Batch("shipment-batch", "RETRO-CLOCK", 100, eta=tomorrow)
    latest = Batch("shipment-batch", "RETRO-CLOCK", 100, eta=later)

    product = Product("RETRO-CLOCK", [earliest, medium, latest])
    line = OrderLine("oref", "RETRO-CLOCK", 10)
    product.allocate(line)

    assert earliest.available_quantity == 90
    assert medium.available_quantity == 100
    assert latest.available_quantity == 100


def test_returns_allocated_batch_ref(tomorrow):
    in_stock_batch = Batch("in-stock-batch", "RETRO-CLOCK", 100, eta=None)
    shipment_batch = Batch("shipment-batch", "RETRO-CLOCK", 100, eta=tomorrow)

    product = Product("RETRO-CLOCK", [in_stock_batch, shipment_batch])
    line = OrderLine("oref", "RETRO-CLOCK", 10)
    allocation = product.allocate(line)

    assert allocation == in_stock_batch.reference


def test_raise_out_of_stock_if_cannot_allocate(today):
    batch = Batch("in-stock-batch", "RETRO-CLOCK", 10, eta=today)
    product = Product("RETRO-CLOCK", [batch])
    line = OrderLine("order", "RETRO-CLOCK", 11)

    with pytest.raises(OutOfStock, match="RETRO-CLOCK"):
        product.allocate(line)
