import pytest


@pytest.fixture
def session():
    """Simulate DB session.
    """

    class FakeSession:
        def __init__(self):
            self.committed = False

        def commit(self):
            self.committed = True

    return FakeSession()
