from allocation.domain import Batch, OrderLine


def test_change_batch_qty():
    sku = "BRITISH-LAMP"
    batch = Batch("my_batch", sku, 10, None)
    line = OrderLine(12, sku, 9)

    batch.allocate(line)

    assert batch.available_quantity == 1
    assert line in batch.allocations

    batch.purchased_quantity = 9
    assert batch.available_quantity == 0
    assert line in batch.allocations

    batch.purchased_quantity = 8
    assert batch.available_quantity == 8
    assert line not in batch.allocations
