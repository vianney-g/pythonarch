"""End to end test with real API.
"""

from datetime import date, timedelta

import pytest
from django.test import Client

from allocate.models import Batch


@pytest.mark.django_db(transaction=True)
def test_complete_allocation(batch_name):
    """Acceptance test, allocation works and is saved.
    """
    batch1, batch2, batch3 = batch_name(), batch_name(), batch_name()
    today = date.today()
    tomorrow = today + timedelta(days=1)
    later = tomorrow + timedelta(days=1)
    sku = "FRENCH-LAMP"

    Batch.objects.create(reference=batch1, sku=sku, qty=100, eta=later)
    Batch.objects.create(reference=batch2, sku=sku, qty=100, eta=tomorrow)
    Batch.objects.create(reference=batch3, sku="other_sku", qty=100, eta=today)

    client = Client()
    response = client.post(
        "/allocate",
        {"order_id": "123", "sku": sku, "qty": 10},
        content_type="application/json",
    )
    assert response.status_code == 200
    assert response.json() == {"batch_ref": batch2}

    saved_batch = Batch.objects.get(reference=batch2)
    assert saved_batch.qty == 100  # batch qty doesn't change
    [order_line] = saved_batch.order_lines.all()
    assert order_line.sku == sku
    assert order_line.order_id == 123
    assert order_line.qty == 10


@pytest.mark.django_db(transaction=True)
def test_400_response_for_out_of_stock(batch_name):
    """No more stock must generate a HTTP 400.
    """
    client = Client()
    sku = "FRENCH_LAMP"
    Batch.objects.create(reference=batch_name(), sku=sku, qty=1, eta=date.today())
    response = client.post(
        "/allocate",
        {"order_id": "123", "sku": sku, "qty": 10},
        content_type="application/json",
    )
    assert response.status_code == 400
    assert response.json()["error"] == f"Out of stock for sku {sku}"


@pytest.mark.django_db(transaction=True)
def test_400_response_for_unknown_sku():
    """Unknown SKU must generate a HTTP 400.
    """
    sku = "UNKNOWN-SKU"
    response = Client().post(
        "/allocate",
        {"order_id": "123", "sku": sku, "qty": 10},
        content_type="application/json",
    )
    assert response.status_code == 400
    assert response.json()["error"] == f"Invalid sku {sku}"


@pytest.mark.django_db(transaction=True)
def test_batch_can_be_created(batch_name):
    """Unknown SKU must generate a HTTP 400.
    """
    sku, batch_id = "FRENCH_LAMP", batch_name()
    response = Client().post(
        "/allocate/batch",
        {"ref": batch_id, "sku": sku, "qty": 10, "eta": "2010-01-12"},
        content_type="application/json",
    )
    assert response.status_code == 200
    assert response.json()["info"] == f"Batch {batch_id} successfully created"
    db_batch = Batch.objects.get(reference=batch_id)
    assert db_batch.sku == sku
    assert db_batch.qty == 10
    assert db_batch.eta == date(2010, 1, 12)


@pytest.mark.django_db(transaction=True)
def test_batch_can_be_updated(batch_name):
    batch_id = batch_name()
    Batch.objects.create(reference=batch_id, sku="sku", qty=12, eta=date.today())
    response = Client().put(
        "/allocate/batch",
        {"ref": batch_id, "qty": 10},
        content_type="application/json",
    )
    assert response.json()["info"] == f"Batch {batch_id} successfully updated"
    batch = Batch.objects.get(reference=batch_id)
    assert batch.qty == 10
